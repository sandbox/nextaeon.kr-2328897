 -- SUMMARY --

 Menu item block module creates block per menu link items for menus.
 You can places one menu item block on drupal.

 -- CONFIGURATION --

 * Configure menus in Administration » User Interface » Menu item block.
   - Selects menu to create block per menu link items.

 * You can see blocks name starts with 'Menulink:' in blocks.
