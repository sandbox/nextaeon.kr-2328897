<?php
/**
 * @file
 * Callbacks for module administration pages.
 */

/**
 * Implements settings form.
 * Administer choose menus to provide blocks.
 */
function menu_item_block_setting($form) {
  $menus = menu_get_menus();

  $form['menu_item_block_setting'] = array(
    '#title' => t('Choose menus'),
    '#type' => 'checkboxes',
    '#description' => t('Select menus to provide blocks per menu link item.'),
    '#options' => $menus,
    '#default_value' => variable_get('menu_item_block_setting', array()),
  );

  return system_settings_form($form);
}
