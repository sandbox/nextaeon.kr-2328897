<?php
/**
 * @file
 * The default template for 'menu_link_tem'.
 *
 * Available variables :
 *
 * - $link: An HTML markup containing a link.
 * - $link_title: Title of given link.
 * - $link_url: URL of given link.
 *
 * @see menu_item_block_preprocess_menu_item_block()
 */
?>
<?php print $link; ?>
